# query-recorder-trees

This is a little tool to help analyse GitLab query recorder logs, and build
flame-graph like trees.

## Usage

```
query-recorder-trees - DB profiling information for GitLab

Usage: query-recorder-trees [--html] [--css-file FILE] [--js-file FILE]
                            [--base-url URL] [--ref-name REF]
  Analyse a Query Recorder log

Available options:
  --html                   Produce an HTML document
  --css-file FILE          CSS file (default: "css/style.css")
  --js-file FILE           JS file (default: "scripts/toggler.js")
  --base-url URL           Base URL of GitLab instance
  --ref-name REF           Branch name or commit SHA for the current
                           head (default: "master")
  -h,--help                Show this help text
```

This tool takes in a query recorder log on standard input, and prints
an analysis to standard output, in plain text format by default, but an
HTML page can be built as well.

Example output: https://alexkalderimis.gitlab.io/query-recorder-trees

## Building and installation

This tool is developed with [stack](https://docs.haskellstack.org/en/stable/README/), so you
will probably want to install that first.

To build a binary:

```
stack build
```

To run it locally (without installation):

```
cat some/log/file.log | stack run
```

To install the executable onto your path:

```
stack install
```

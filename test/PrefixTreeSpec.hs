module PrefixTreeSpec where

import Test.Hspec
import Test.QuickCheck

import Data.Foldable
import Data.List.NonEmpty (NonEmpty)
import Data.Monoid
import PrefixTree as P

spec :: Spec
spec = describe "PrefixTree" $ do
  let key :: String -> String
      key = id
      value :: Int -> Int
      value = id
  specify "a singleton contains its key" $ property $ \k v ->
    member k (P.singleton (key k) (v :: Int))
  specify "a singleton only contains its key" $ property $ \c k v ->
    not . member (c : k) $ P.singleton (key k) (v :: Int)
  specify "a singleton does not contain intermediate values" $ property $ \a b c v ->
    not . member [a, b] $ P.singleton (key [a, b, c]) (v :: Int)
  specify "inserting a value retrieves the same value" $ property $ \k k' v v' ->
    let t = P.insert k' v' (P.singleton (key k) (value v))
     in P.lookup k' t == Just v'
  specify "inserting a value retrieves the same value, no matter how many keys" $ property $ \kvs k' v' ->
    let t = P.insert (key k') (value v') (foldl' (\t (k,v) -> P.insert k v t) P.empty (map id kvs))
     in P.lookup k' t == Just v'
  specify "insertWith combines values" $ property $ \k v v' ->
    let t = P.insertWith (+) k v' (P.singleton (key k) (value v))
     in P.lookup k t == Just (v + v')
  specify "<> is the same as insertWith mappend" $ property $ \k v v' ->
    let t = P.singleton (key k) (Sum $ value v) <> P.singleton k (Sum  v')
     in P.lookup k t == Just (Sum (v + v'))


{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Lib as L
import qualified Render as R

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text.IO as TIO
import Data.Tree (drawForest)

import Options.Applicative
import Data.Semigroup

data OutputFormat = HTML | PlainText
  deriving (Eq, Show)

data Options = Options
  { format :: OutputFormat
  , cssFile :: String
  , jsFile :: String
  , baseURL :: R.BaseURL
  , refName :: R.RefName
  }
  deriving (Show, Eq)

options :: Parser Options
options = Options <$> flag PlainText HTML (long "html" <> showDefault <> help "Produce an HTML document")
                  <*> strOption (long "css-file" <> metavar "FILE"
                                  <> help "CSS file"
                                  <> showDefault
                                  <> value "css/style.css")
                  <*> strOption (long "js-file" <> metavar "FILE"
                                 <> help "JS file"
                                 <> showDefault
                                 <> value "scripts/toggler.js")
                  <*> fmap R.BaseURL (strOption (long "base-url" <> metavar "URL"
                                 <> help "Base URL of GitLab instance"
                                 <> value "https://gitlab.com/gitlab-org/gitlab"))
                  <*> fmap R.RefName (strOption (long "ref-name" <> metavar "REF"
                                 <> help "Branch name or commit SHA for the current head"
                                 <> value "master" <> showDefault))

main :: IO ()
main = do
  opts <- execParser pinfo
  fg <- readInput
  process opts fg
  where
    readInput = fmap L.toFlameGraph . L.parse <$> TIO.getContents
    pinfo = info (options <**> helper)
                 (fullDesc
                 <> progDesc "Analyse a Query Recorder log"
                 <> header "query-recorder-trees - DB profiling information for GitLab")

process :: Options -> Either String L.FlameGraph -> IO ()
process _    (Left err) = putStrLn err
process opts (Right fg) = case format opts of
    PlainText -> putStrLn . R.text $ fg
    HTML -> LBS.putStr =<< (R.html fg (baseURL opts) (refName opts) <$> read cssFile <*> read jsFile)
  where
    read f = TIO.readFile (f opts)

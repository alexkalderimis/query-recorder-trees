{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DerivingVia #-}

module Render (text, html, BaseURL(..), RefName(..)) where

import Data.String
import qualified Data.List.NonEmpty as NE
import Data.List.NonEmpty (NonEmpty)
import Control.Monad (when, forM_)
import Text.Printf (printf, PrintfArg)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Tree (Tree(..), Forest, drawForest)

import Text.Blaze.Html5 as H hiding (map, html, text)
import Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import qualified Data.ByteString.Lazy as L

import Lib
import PrefixTree (tree)

type CallSites = NonEmpty CallSite
type SQL = Text
newtype BaseURL = BaseURL String deriving (Show, Eq, PrintfArg, IsString) via String
newtype RefName = RefName String deriving (Show, Eq, PrintfArg, IsString) via String
type Analysis = Forest (CallSite, Duration, Int, [SQL])
type CollapsedAnalysis = Forest (CallSites, Duration, Int, [SQL])

html :: FlameGraph -> BaseURL -> RefName -> Text -> Text -> L.ByteString
html g base ref css js = renderHtml . docTypeHtml $ do
  let t = asTree g

  H.head $ do
    H.title "Query recorder analysis"
  H.link ! A.href "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
         ! A.rel "stylesheet"
         ! customAttribute "integrity" "sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
         ! customAttribute "crossorigin" "anonymous"
  H.link ! A.rel "stylesheet"
         ! A.href "https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.7.2/build/styles/default.min.css"
  H.script ! A.src "https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.7.2/build/highlight.min.js"
           $ pure ()
  H.script ! A.src "https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.7.2/build/languages/sql.min.js"
           $ pure ()
  H.style (preEscapedText css)

  body (ul . renderNode base ref $ collapse t)

  H.script (preEscapedText "hljs.highlightAll();")
  H.script (preEscapedText js)

collapse :: Analysis -> CollapsedAnalysis
collapse = fmap go
  where go tree = let (cs, d, n, qs) = rootLabel tree
                  in case (qs, subForest tree) of
                     ([], [kid]) -> let collapsed = go kid
                                        (sites, d, n, qs) = rootLabel collapsed
                                     in collapsed { rootLabel = (NE.cons cs sites, d, n, qs) }
                     (_, kids) -> Node (pure cs, d, n, qs) (collapse kids)

renderNode :: BaseURL -> RefName -> CollapsedAnalysis -> Html
renderNode base ref = mapM_ go
  where
    go (Node (callsites, duration, n, queries) children) = do
      H.li $ do
        sectionHeading base ref callsites children
        H.div ! class_ "card" $ do
          H.div ! class_ "card-body" $ do
            when (length callsites > 1) $ do
              H.details $ do
                H.summary (string . printf "%d frames" $ length callsites)
                ol $ forM_ callsites (H.li . H.span . callsiteHtml base ref)
            table ! class_ "table table-sm" $ do
              thead $ do
                tr $ do
                  th "Duration"
                  th "Total number of queries"
                  th "Number of queries in this frame"
              tbody $ do
                tr $ do
                  td (string $ printf "%.2fms" duration)
                  td (toHtml n)
                  td (toHtml $ length queries)
            forM_ queries (p . renderSQL)
        ul ! class_ "nested" $ forM_ children go

sectionHeading :: BaseURL -> RefName -> CallSites -> [a] -> Html
sectionHeading base ref callsites children =
  let heading = if NE.length callsites > 1
                then H.span $ do
                        callsiteHtml base ref (NE.head callsites)
                        H.span ".."
                        callsiteHtml base ref (NE.last callsites)
                else callsiteHtml base ref $ NE.head callsites
      cssClass = if null children then "leaf" else "caret"
   in H.span ! class_ cssClass $ heading

callsiteHtml :: BaseURL -> RefName -> CallSite -> Html
callsiteHtml base ref cs = H.code (H.a ! A.href (fromString blobUrl) $ toHtml (show cs))
  where blobUrl = printf "%s/-/blob/%s/%s#L%d" base ref (fileName cs) (lineNo cs)

renderSQL :: SQL -> Html
renderSQL = pre . (code ! class_ "lang-sql") . toHtml

text :: FlameGraph -> String
text = drawForest . map (fmap str) . asTree
  where
    str (callsite, dur, n, queries) = printf "%s ==> Total duration: %.2fms, Query count: %d" (show callsite) (dur * 1000.0) n
                                      ++ "\n" ++ T.unpack (T.intercalate "\n" queries)

asTree :: FlameGraph -> Analysis
asTree = map (summarize . fmap unmaybe) . behead . tree . getTree
  where
    behead (Node (Left _, Nothing) forest) = map (fmap (\(Right cs, mv) -> (cs, mv))) forest

    unmaybe (callsite, Nothing) = (callsite, [])
    unmaybe (callsite, Just details) = (callsite, details)

    summarize (Node (callsite, details) children) =
      let summarized = fmap summarize children
          total = sum (fmap fst details)
          queries = fmap snd details
          childTotal = sum [dur | (Node (_, dur, _, _) _) <- summarized]
          queryCount = sum [n   | (Node (_, _, n, _) _) <- summarized]
       in Node (callsite, total + childTotal, length queries + queryCount, queries) summarized

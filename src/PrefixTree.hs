{-# LANGUAGE DeriveFunctor #-}

module PrefixTree (
  PrefixTree,
  singleton,
  empty,
  tree,
  set,
  modify,
  insert,
  insertWith,
  delete,
  member,
  lookup,
) where

import Prelude hiding (lookup)
import Data.Foldable
import Data.Maybe (isJust)
import qualified Data.List.NonEmpty as NE
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.Tree as Tree

data PrefixTree k a = PT { rootValue :: (Maybe a), nodes :: [PTNode k a] }
  deriving (Show, Eq, Functor)

data PTNode k a = T { key :: k, value :: Maybe a, children :: [PTNode k a] }
  deriving (Show, Eq, Functor)

data RootKey = RootKey
  deriving (Show, Eq)

data Zipper k a = Z
  { parent :: Maybe (Zipper k a)
  , focus :: (Either RootKey k, Maybe a)
  , lefts :: [PTNode k a]
  , rights :: [PTNode k a]
  , kids :: [PTNode k a]
  }

instance (Ord k, Semigroup a) => Semigroup (PrefixTree k a) where
  (<>) = merge (<>)

instance (Ord k, Semigroup a) => Monoid (PrefixTree k a) where
  mempty = empty
  mappend = (<>)

tree :: PrefixTree k a -> Tree.Tree (Either RootKey k, Maybe a)
tree (PT mv kids) = Tree.Node (Left RootKey, mv) (map ntree kids)

ntree :: PTNode k a -> Tree.Tree (Either RootKey k, Maybe a)
ntree node = Tree.Node (Right (key node), value node) (map ntree $ children node)

singleton :: [k] -> a -> PrefixTree k a
singleton [] value = empty { rootValue = Just value }
singleton path value = empty { nodes = [node path value] }
  where 
    node [k] value = T k (Just value) []
    node (k:ks) value = T k Nothing [node ks value]

empty :: PrefixTree k a
empty = PT Nothing []

zipper :: PrefixTree k a -> Zipper k a
zipper tree = Z Nothing (Left RootKey, rootValue tree) [] [] (nodes tree)

up :: Zipper k a -> Maybe (Zipper k a)
up (Z mp (ek, a) lhs rhs downs) = do
  p <- mp
  let (Right k) = ek -- must be a key if p is Some
  let curr = T k a downs
  pure $ p { kids = lhs ++ curr : rhs }

fromZipper :: Zipper k a -> PrefixTree k a
fromZipper z = case up z of
  Nothing -> case focus z of
               (Left RootKey, a) -> PT a (kids z)
               (Right k, a) -> empty { nodes = [T k a (kids z)] }
  Just p -> fromZipper p

downTo :: Eq k => Zipper k a -> k -> Zipper k a
downTo z k =
  let (lhs, kids') = span ((/= k) . key) (kids z)
      parent = Just $ z { kids = [] }
   in case kids' of
     []        -> Z parent (Right k, Nothing) lhs [] []
     (kid:rhs) -> Z parent (Right k, value kid) lhs rhs (children kid)

descendTo :: Eq k => [k] -> PrefixTree k a -> Zipper k a
descendTo path tree = foldl' downTo (zipper tree) path

set :: a -> Zipper k a -> Zipper k a
set a = modify (pure . pure a)

modify :: (Maybe a -> Maybe a) -> Zipper k a -> Zipper k a
modify f z = let (k, ma) = focus z in z { focus = (k, f ma) }

insert :: Eq k => [k] -> a -> PrefixTree k a -> PrefixTree k a
insert path a = fromZipper . set a . descendTo path

member :: Eq k => [k] -> PrefixTree k a -> Bool
member = (isJust .) . lookup

lookup :: Eq k => [k] -> PrefixTree k a -> Maybe a
lookup path = snd . focus . descendTo path

insertWith :: Eq k => (a -> a -> a) -> [k] -> a -> PrefixTree k a -> PrefixTree k a
insertWith f path a = fromZipper . modify (pure . maybe a (`f` a)) . descendTo path

delete :: Eq k => [k] -> PrefixTree k a -> PrefixTree k a
delete path = fromZipper . modify (pure Nothing) . descendTo path

merge :: Eq k => (a -> a -> a) -> PrefixTree k a -> PrefixTree k a -> PrefixTree k a
merge f lhs = foldl' (\tree (ks, a) -> insertWith f ks a tree) lhs . decompose

decompose :: PrefixTree k a -> [([k], a)]
decompose tree =
   [([], a) | Just a <- [rootValue tree]] <> (nodes tree >>= decomposeNodes)

decomposeNodes :: PTNode k a -> [([k], a)]
decomposeNodes node = let k = key node in 
   [([k], a) | Just a <- [value node]] <> fmap (\(p, a) -> (k:p, a)) (children node >>= decomposeNodes)

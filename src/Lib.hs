{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Lib where

import PrefixTree

import Data.List (nub, find)
import Data.Foldable
import Data.Maybe
import Data.Attoparsec.Text
import Control.Applicative
import Data.Monoid
import Data.Map.Strict (Map)
import qualified Data.Set as Set
import qualified Data.Map.Strict as M
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.List.NonEmpty as NE
import Data.List.NonEmpty (NonEmpty)

data CallSite = Frame { fileName :: Text, lineNo :: Int, codeRef :: Text }
  deriving (Eq, Ord)

instance Show CallSite where
  show (Frame fn ln code) = T.unpack fn ++ ":" ++ show ln ++ ":in `" ++ T.unpack code ++ "'"

type Duration = Double

data QueryExecution = QueryExecution
    { sql :: Text
    , duration :: Duration
    , backtrace :: NonEmpty CallSite
    }
  deriving (Show)

newtype FlameGraph = FlameGraph { getTree :: PrefixTree CallSite [(Duration, Text)] }
  deriving (Semigroup, Monoid)

toFlameGraph :: [QueryExecution] -> FlameGraph
toFlameGraph = foldMap $ \qe ->
  FlameGraph (singleton (NE.toList . NE.reverse $ backtrace qe) [(duration qe, sql qe)])

parse :: Text -> Either String [QueryExecution]
parse = parseOnly (many queryExecutionP <* endOfInput)

queryExecutionP :: Parser QueryExecution
queryExecutionP =
  QueryExecution <$> (T.intercalate "\n" <$> some sqlFragmentP)
                 <*> durationP
                 <*> (NE.fromList <$> some callSiteP)

durationP :: Parser Duration
durationP = string "QueryRecorder DURATION:  --> " *> double <* endOfLine

sqlFragmentP :: Parser Text
sqlFragmentP = string "QueryRecorder SQL:  --> " *> takeTill isEndOfLine <* endOfLine

-- eg. spec/spec_helper.rb:347:in `block (2 levels) in <main>'
callSiteP :: Parser CallSite
callSiteP = string "QueryRecorder backtrace:  --> " *> frame <* (string "'" >> endOfLine)
 where
   frame = Frame <$> takeWhile1 (/= ':')
                 <*> (string ":" *> decimal)
                 <*> (string ":in `" *> takeWhile1 (/= '\''))
